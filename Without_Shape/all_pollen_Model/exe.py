import cv2;
import numpy as np;
import sys;
import os,glob;
sys.path.append("lib");
import lib_GaborWavelets as GW;
import lib_FourierTransform as FT;
import lib_LBP as LBP;
import lib_glcm as GLCM;
import lib_HOG as HOG;
import lib_sfta as SFTA;
import multiprocessing;
import lib_Haralick as HAR;
sys.path.append("lib_apertures");
import exe_all_toDetect as apertures;
from datetime import datetime;
import time;

def eachFile(file,nProcess,return_dict,type,totalPart):
	tab = [type];
	tab1 = GW.main(file);
	tab2 = FT.main(file);
	tab3 = LBP.main(file);
	tab4 = GLCM.main(file);
	tab5 = HOG.main(file);
	tab6 = HAR.main(file);
	tab7 = apertures.main(file,nProcess);
	
	for element in tab1:
		tab.append(element);

	for element in tab2:
		tab.append(element);
	
	for element in tab3:
		tab.append(element);
		
	for element in tab4:
		tab.append(element);
		
	for element in tab5:
		tab.append(element);
		
	for element in tab6:
		tab.append(element);
		
	for element in tab7:
		tab.append(element);
		
	return_dict[totalPart] = tab;

def createCategorieTable(folders,picExtension):
	# Shared variable
	manager = multiprocessing.Manager();
	return_dict = manager.dict();
	
	type = 0;
	
	totalPart = 0;

	#for all the folders
	for folder in folders:
		dir = os.path.join(folder,"*."+picExtension);
		cpu = multiprocessing.cpu_count();
		limitProcess = cpu-1;
		
		#store the files
		tabFichier = glob.glob(dir);
		print tabFichier;
		#size of the tab
		total = len(tabFichier);
		#number of particles
		nbP = 0;
		
		#number of tens
		while(nbP < total):
			cpt = 0;
			tabP = [];
			#for all the numbers in a ten
			while(cpt < limitProcess):
				if(nbP < total):
					tabP.append(multiprocessing.Process(target=eachFile, args=(tabFichier[nbP],nbP,return_dict,type,totalPart)));
					totalPart = totalPart + 1;
					
				#increasing number of particles
				nbP = nbP + 1;
				cpt = cpt + 1;
			
			for process in tabP:
				process.start();
				
			for process in tabP:
				print process;
				process.join();
		
		type = type + 1;
	return return_dict.values();

def saveFile(fName,array,newLine):
	file = '';
	for line in array:
		for case in line:
			file = file + str(case) + ",";
		file = file[:-1] + newLine;
	f = open(fName, 'w');
	f.write(file);
	f.close();
	
def saveFileText(fName,str):
	f = open(fName, 'w');
	f.write(str);
	f.close();
	
#-------------------------------------------- MAIN

if __name__ == '__main__':
	now1 = datetime.now();
	print now1;
	folderTab = [',Alder',',Birch',',Hazel',',Mugwort',',Sweet-Grass']
	outFile = '../MODEL.csv'
	tab = createCategorieTable(folderTab,'tif');
	saveFile(outFile,tab,"\n");
	now2 = datetime.now();
	print now2;
	timeExec = now2-now1;
	print timeExec;
	saveFileText("Execution_Time.txt",str(timeExec))
#-------------------------------------------- EXEC