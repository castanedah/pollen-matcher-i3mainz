#Alder
	# 5 apertures
#Birch
	# 3 apertures
#Hazel
	# 3 apertures
#Mugwort
	# 3 traits
#Sweet Grass
	# ??
	
import cv2;
import numpy as np;

#Move a window all over the image, save the different states of the window
def deplaceFenetre(path,type,step,nProcess):

	#Modify the size, according to the type
	if(type == "Hazel"):
		size = 50;
	else:
		size = 40;
		
	mask = np.zeros((size,size))

	#Read the image, compute the image
	img = cv2.imread(path);
	(height,width,trois) = img.shape;
	
	count1 = 'A';
	count2 = 'A';
		
	#Moving all over the image
	for i in range(30,height-size+1,step):
		for j in range(30,width-size+1,step):
			#select the area
			mask = img[i:i+size,j:j+size,:];
			#save the current image
			cv2.imwrite("tmp/temp"+str(nProcess)+"/img"+count2+count1+".jpg",mask);
			#change the name
			count1 = chr(ord(count1) + 1);
		count2 = chr(ord(count2) + 1);	
		count1 = 'A';