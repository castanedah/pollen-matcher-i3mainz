import os,glob;
import sys;
sys.path.append("libs");
import lib_all_exe as exe;
import csvManager as cm;

def main(file,nProcess):
	# Return the array of aperture detection
	result = exe.main(None,file,nProcess);
	tabUniq = [];
	
	# Put in a 1D array
	if(result != "No one aperture found"):
		for liste in result:
			for element in liste:
				tabUniq.append(element);
		
	#Want a 40 elements array
	taille = len(tabUniq);
	if(taille < 40):
		rest = 40-taille;
		for i in range(0,rest):
			tabUniq.append("0");
			
	return tabUniq;