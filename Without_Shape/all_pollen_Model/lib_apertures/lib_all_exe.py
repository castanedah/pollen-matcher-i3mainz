import os, sys, shutil;
sys.path.append("libs")
import lib_deplacerFenetre as df;
import csvScore as csvS;
import csvScoreProba as csvP;
import create_csv as cc;

#begin the process of the aperture detection
def main(type,path,nProcess):
	step = 10;

	#we delete the folder if it exists
	if os.path.exists("tmp/temp"+str(nProcess)):
		shutil.rmtree("tmp/temp"+str(nProcess));

	#creation of the folder
	os.makedirs("tmp/temp"+str(nProcess));
	
	if os.path.exists("resultApertures/apertures"+str(nProcess)):
		shutil.rmtree("resultApertures/apertures"+str(nProcess));
		
	os.makedirs("resultApertures/apertures"+str(nProcess));

	#select the model
	modelFile = "MODEL_GENERAL.csv";
	scoreFile = "SCORES"+str(nProcess)+".csv";

	#creation of the windows
	df.deplaceFenetre(path,type,step,nProcess);

	#Put in csv the different images created
	cc.to_class_csv(nProcess);

	#Finding an aperture. Return the number of apertures
	number = csvS.main(scoreFile,modelFile,type,1,nProcess);
	
	# os.remove("lib_apertures/"+scoreFile);

	#If we have at least an element
	if (number <> 0):

		#Put in csv the different images created
		cc.yes_score_csv(nProcess);

		modelFile = 'MODEL_YES.csv';
		scoreFile = "SCORES_YES"+str(nProcess)+".csv";

		#Finding the right class for all the apertures found
		result = csvP.main(scoreFile,modelFile);
		
		# os.remove("lib_apertures/"+scoreFile);
		
		return result;
	else:
		return "No one aperture found";