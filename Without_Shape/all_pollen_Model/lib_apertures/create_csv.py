import cv2;
import numpy as np;
import sys;
import os,glob;
sys.path.append("libs");
import lib_GaborWavelets as GW;
import lib_FourierTransform as FT;
import lib_LBP as LBP;

# Create the csv to save
def createCategorieTable(folders,picExtension):
	ret = [];
	type = 0;
	#For all the folder given
	for folder in folders:
		dir = os.path.join(folder,"*."+picExtension);
		
		#for all the files in a folder:
		for file in glob.glob(dir):
			#Store 0 for "No", 1 for "Yes" 
			tab = [type];
			#Made the transformations
			tab1 = GW.main(file);
			tab2 = FT.main(file);
			tab3 = LBP.main(file);
			
			#all the array 1D
			for element in tab1:
				tab.append(element);

			for element in tab2:
				tab.append(element);
				
			for element in tab3:
				tab.append(element);
				
			ret.append(tab);
	return ret;	

#Save to csv, with an array 2D.
def saveFile(fName,array,newLine):
	file = '';
	for line in array:
		for case in line:
			file = file + str(case) + ",";
		file = file[:-1];
		file = file + newLine;
	f = open(fName, 'w');
	f.write(file);
	f.close();
	
#-------------------------------------------- MAIN
def yes_score_csv(nProcess):
	folderTab = ["resultApertures/apertures"+str(nProcess)];
	outFile = "lib_apertures/SCORES_YES"+str(nProcess)+".csv"
	file = createCategorieTable(folderTab,'jpg');

	saveFile(outFile,file,'\n');

def to_class_csv(nProcess):
	# print "Creating CSV..."
	folderTab = ["tmp/temp"+str(nProcess)];
	outFile = "lib_apertures/SCORES"+str(nProcess)+".csv";
	file = createCategorieTable(folderTab,'jpg');

	saveFile(outFile,file,'\n');