import numpy as np;
import os, glob;
import math;
from numpy import savetxt;
from sklearn import cross_validation, svm, neighbors;
from sklearn.ensemble import RandomForestClassifier;
from sklearn.naive_bayes import GaussianNB;
from matplotlib import pyplot as plt;
import cv2;

#read in data, parse into training and target sets
# although using "all_better2.csv" works fine: gives results of 100% due to distinguishable background
# bool : 0 if we want to do the restriction, 1 else
def main(toClassFile,modelFile):

	modelFile = "lib_apertures/"+modelFile;
	toClassFile = "lib_apertures/"+toClassFile;
	
	#loading data : the file to sort first, and the model then
	dataset_toClass = np.genfromtxt(open(toClassFile), delimiter=',', dtype='f8')[:]
	#number of columns (CAUTION)
	if(len(dataset_toClass)==105):
		data_toClass = np.array(dataset_toClass[1:]);
	else:
		data_toClass = np.array([x[1:] for x in dataset_toClass]);
	
	dataset_model = np.genfromtxt(open(modelFile), delimiter=',', dtype='f8')[1:]
	target = np.array([x[0] for x in dataset_model]);
	train = np.array([x[1:] for x in dataset_model]);

	#define X & y, like in lessons
	X,y = train, target

	#use support vector machines classifier
	#use linear, because other are too slow
	clf = svm.SVC(kernel='linear', C=1, probability=True)
	
	# total length of the train
	n_sample = len(X)

	# permutation, because classes are in good order
	np.random.seed(0)
	order = np.random.permutation(n_sample)
	X = X[order]
	y = y[order].astype(np.float)

	# define X_train and y_train, respectively train and target
	X_train = X[:]
	y_train = y[:]
	# define the test : so the class table
	X_test = data_toClass[:]
	
	# define the model
	clf = clf.fit(X_train,y_train)
	# predict, and affect a class value to the test data
	resA = clf.predict_proba(X_test);
	
	#cross_validation
	return resA;