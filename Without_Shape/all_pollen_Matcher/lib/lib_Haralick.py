import cv2;
import numpy as np
import os 
import mahotas
import mahotas.features
from matplotlib import pyplot as plt
import csvManager
# import logging 
# from .texture import haralick
# add two matrix into one matrix

# return a matrix which contain the elements of arrays tab and tab2 
def addTab(tab,tab2):
	res=[];
	for i in xrange(len(tab)):
		res.append(tab[i])
	for j in xrange(len(tab2)):
		res.append(tab2[j])
	return res	
	
# @param tab matrix, tab2 matrix
# add element of tab into tab2 

def main(file):
	img = mahotas.imread(file,0)
	# return a matrix of 2 dimension which contain 13 arrays 
	ret=mahotas.features.haralick(img)#.mean(0)

	# incorporate the result of 
	for i in xrange(12):
		# tab=img[i]
		res=addTab(ret[i],ret[i+1])

	# generate a csv file with the matrix res 
	return res;