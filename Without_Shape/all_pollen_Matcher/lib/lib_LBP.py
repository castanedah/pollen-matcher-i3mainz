import cv2
import numpy as np
import os
import math
import peak
import csv
import csvManager
import scipy
from scipy.signal import convolve2d
# import numpy; print numpy.__version__
# import scipy; print scipy.__version__

# @param X is an image
	
def lbp(X):
	# for more informations: http://www.bytefish.de/blog/local_binary_patterns/
	# compare the pixel with his 8 neighbor
	#|1|2|3|	|0|0|0|
	#|9|5|6| => |1| |1| 
	#|5|3|1|	|1|0|0|
	
	X = np.asarray(X)
	X = (1<<7) * (X[0:-2,0:-2] >= X[1:-1,1:-1]) \
		+ (1<<6) * (X[0:-2,1:-1] >= X[1:-1,1:-1]) \
		+ (1<<5) * (X[0:-2,2:] >= X[1:-1,1:-1]) \
		+ (1<<4) * (X[1:-1,2:] >= X[1:-1,1:-1]) \
		+ (1<<3) * (X[2:,2:] >= X[1:-1,1:-1]) \
		+ (1<<2) * (X[2:,1:-1] >= X[1:-1,1:-1]) \
		+ (1<<1) * (X[2:,:-2] >= X[1:-1,1:-1]) \
		+ (1<<0) * (X[1:-1,:-2] >= X[1:-1,1:-1])
	return X	

# add two matrix into one matrix
def addTab(tab,tab2):
	res=[];
	for i in xrange(len(tab)):
		res.append(tab[i])
	for j in xrange(len(tab2)):
		res.append(tab2[j])
	return res	
	
# add the two parammeters into a matrix
# tab is matrix
# var is a number
def addTab2(tab,var):
	res=[];
	for i in xrange(len(tab)):
		res.append(tab[i])
	res.append(var)
	
def average(tab):
    return sum(tab, 0.0) / len(tab)

def gradian_magnitude(x,y):
	return math.sqrt(math.pow(x,2.0)+math.pow(y,2.0))

def tabToFloat(tab):
	for i in xrange(len(tab)):
		np.float(tab[i])
	
def tabToInt(tab):
	for i in xrange(len(tab)):
		np.int(tab[i])	
	return tab
	
def main(file):
	img=cv2.imread(file,0);

	res=lbp(img);

	# create an matrix of 2 dimention called hist which contain 
	# the percentage and the color degres of the image res
	hist=np.histogram(res,density=True)

	# extract the peaks of the image's histogram
	(val ,percentage)= peak.findPeaks2(hist[0],hist[1]);
	(val ,percentage)= peak.restrictPeak(val,percentage,2);

	# compute the average of image's pixel
	averageVal =average(val)
	averagePercentage = average(percentage)

	res=addTab(val,percentage)
	res=tabToInt(res)

	# this is the skew of the histogram
	skew=scipy.stats.skew(val)

	res2=addTab2(res,skew)

	kurtosis = scipy.stats.kurtosis(val)

	#res.append(kurtosis)

	return res;