import numpy as np
from numpy import savetxt
import sys;
from sklearn import cross_validation, svm, neighbors
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB

#read in data, parse into training and target sets
# although using "all_better2.csv" works fine: gives results of 100% due to distinguishable background

def main(csvOut,model,sortedCsv):
	#loading data : the file to sort first, and the model then
	print 'loading "'+csvOut+'"';print'';
	
	dataset_toClass = np.genfromtxt(open(csvOut), delimiter=',', dtype='f8')[:]
	print dataset_toClass;
	data_toClass = np.array([x[1:] for x in dataset_toClass])
		
	dataset_model = np.genfromtxt(open(model), delimiter=',', dtype='f8')[:]
	target = np.array([x[0] for x in dataset_model])
	train = np.array([x[1:] for x in dataset_model])

	#use support vector machines classifier
	#use linear, because other are too slow
	print 'Creating class model (might take a while)...';print '';

	clf = svm.SVC(kernel='linear', C=1)

	# total length of the train
	n_sample = len(train)

	# permutation, because classes are in good order
	np.random.seed(0)
	order = np.random.permutation(n_sample)
	train = train[order]
	target = target[order].astype(np.float)

	# define X_train and y_train, respectively train and target
	X_train = train[:]
	y_train = target[:]
	# define the test : so the class table
	X_test = data_toClass[:]

	# define the model
	clf = clf.fit(X_train,y_train)

	# predict, and affect a class value to the test data
	resA = clf.predict(X_test);

	#display the final value

	print 'Matching ...';
	#modify the toClassFile (not directly, store into another file), to add the classes
	for i in range(0,len(resA)):
		dataset_toClass[i][0] = int(resA[i]);
		print "image",i,": class", int(resA[i]);

	'''
	scores = cross_validation.cross_val_score(clf, train, target, cv=10)
	print scores
	print "Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() / 2)
	'''
	#save into a csv file
	print '';print 'Saving csv result';
	savetxt(sortedCsv, dataset_toClass, delimiter=',', fmt='%f');
'''
	normalResult = np.genfromtxt(open("normalResult.csv"), delimiter='\n', dtype='f8')[:];

	total = len(resA);
	count = 0;
	for i in range(0,total):
		if(resA[i] == normalResult[i]):
			count = count + 1;
			
	print str(count)+"/"+str(total);
	print "percentage :", (float(count)/total)*100;
'''