import sys,os;

def tryExists(dir,path,extension):
	if(os.path.exists(dir+path+extension)):
		path = path+'_';
		return tryExists(dir,path,extension);
	else:
		return path;
		
sorted = sys.argv[1];
mosin = sys.argv[2];
toClass = sys.argv[3];


mosinTable = [];
sortedTable = [];

print ' loading MOSIN...';
with open(mosin) as f:
    for line in f:
		line = line.replace('\n','');
		parts = line.split(',');
		mosinTable.append(parts[1].replace('_',''));

print ' loading SORTED...';		
with open(sorted) as f:
    for line in f:
		line = line.replace('\n','');
		parts = line.split(',');
		sortedTable.append(parts[0]);

print ' loading IMAGES...';		
images = [];
for obj in os.listdir(toClass):
	if(obj.find('.tif')):
		images.append(obj);
		
print ' Transforming...';	
for i in range(0,len(images)):
	mosinIndex = sortedTable[i];
	mosinIndex = int(float(mosinIndex));
	type = mosinTable[mosinIndex];
	file = images[i];
	type = tryExists(toClass,type,'.tif');
	os.rename(toClass+file, toClass+type+'.tif');
print ' Transform done';	
		