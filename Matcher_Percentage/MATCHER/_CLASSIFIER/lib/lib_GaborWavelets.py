import numpy as np
import cv2
from multiprocessing.pool import ThreadPool
import csvManager as cm
import math

def main(file):
	#website : https://github.com/kipr/opencv/blob/master/samples/python2/gabor_threads.py
	#construct the filter in 8 directions, with a size of ksize
	def build_filters(ksize):
		filters = []
		for theta in np.arange(0, np.pi, np.pi / 8):
			kern = cv2.getGaborKernel((ksize, ksize), 4.0, theta, 10.0, 0.5, 0, ktype=cv2.CV_32F)
			kern /= 1.5*kern.sum()
			filters.append(kern)
		return filters

	#process the transform on the image, with some filters given
	def process_filters(img, filters):
		accum = np.zeros_like(img)
		for kern in filters:
			fimg = cv2.filter2D(img, cv2.CV_8UC3, kern)
			np.maximum(accum, fimg, accum)
		return accum

	#process the transform on the image, with 1 filter given
	def process_filter(img, filter):
		accum = np.zeros_like(img)
		fimg = cv2.filter2D(img, cv2.CV_8UC3, filter)
		np.maximum(accum, fimg, accum)
		return accum

		
	#process the transform_threaded on the image
	def process_threaded(img, filters, threadn = 8):
		accum = np.zeros_like(img)
		def f(kern):
			return cv2.filter2D(img, cv2.CV_8UC3, kern)
		pool = ThreadPool(processes=threadn)
		for fimg in pool.imap_unordered(f, filters):
			np.maximum(accum, fimg, accum)
		return accum
		
	#for the csv save
	array_csv = [];

	#load image
	img = cv2.imread(file)

	#in gray
	img1 = cv2.imread(file,0)

	# initialization
	all_filters = [];
	all_images = [];

	#filters construction
	all_filters.append(build_filters(3));
	all_filters.append(build_filters(6));
	all_filters.append(build_filters(13));
	all_filters.append(build_filters(28));
	all_filters.append(build_filters(58));

	#in all filters (5*8)
	for filters in all_filters:
		#for each filter in filters
		for f in filters:
			all_images.append(process_filter(img1, f));
		
	i = 0;
	#draw each images
	for img_val in all_images:
		
		#for saving image
		#str0 = "img" + str(i) + ".csv";
		#cm.saveCSVfile2(str0,img_val);
		
		#name the images by size
		if (i/8==0):
			title = "size : 3";
		elif (i/8==1):
			title = "size : 6";
		elif (i/8==2):
			title = "size : 13";
		elif (i/8==3):
			title = "size : 28";
		else:
			title = "size : 58";
			
		#name the images by direction
		if(i%8==0):
			title = title, "direction : 0"
		elif(i%8==1):
			title = title, "direction : pi/8"
		elif(i%8==2):
			title = title, "direction : pi/4"
		elif(i%8==3):
			title = title, "direction : 3pi/8"
		elif(i%8==4):
			title = title, "direction : pi/2"
		elif(i%8==5):
			title = title, "direction : 5pi/8"
		elif(i%8==6):
			title = title, "direction : 3pi/4"
		else:
			title = title, "direction : 7pi/8"
			
		i = i+1;

	#initialization of local energy and mean amplitude
	local_energy = np.zeros((1,40));
	mean_amplitude_tab = np.zeros((1,40));

	ind = 0;
	#for all the images, we compute the local energy and the mean amplitude
	for img_val in all_images:
		(height,width) = img_val.shape;
		#for each pixel of each images
		for i in range(0,height):
			for j in range(0,width):
				local_energy[0,ind] = local_energy[0,ind] + (img_val[i][j] ** 2);
				mean_amplitude_tab[0,ind] = mean_amplitude_tab[0,ind] + img_val[i,j];
				
		#mean_amplitude[0,ind] = np.mean(img_val);
		ind = ind + 1;
		

	#compute the result of the mean amplitude
	mean_amplitude = np.mean(mean_amplitude_tab);

	#for each 8 direction, we took the local energy for all the images with this direction
	local_energy_direction = np.zeros((1,8));

	for i in range(0,8):
		j = i;
		#maximum : 40 images
		while (j < 8*len(all_filters)):
			local_energy_direction[0,i] = local_energy_direction[0,i] + local_energy[0,j];
			j = j+8;

	#research of the maximum
	maxInd = 0;
	maxVal = local_energy_direction[0,0];
	for i in range(1,8):
		if(maxVal < local_energy_direction[0,i]):
			maxInd = i;
			maxVal = local_energy_direction[0,i];
		
	#display the result
	if (maxInd == 0):
		max_direction = 0;
	elif (maxInd == 1):
		max_direction = math.pi/8;
	elif (maxInd == 2):
		max_direction = math.pi/4;
	elif (maxInd == 3):
		max_direction = 3*math.pi/8;
	elif (maxInd == 4):
		max_direction = math.pi/2;
	elif (maxInd == 5):
		max_direction = 5*math.pi/8;
	elif (maxInd == 6):
		max_direction = 3*math.pi/4;
	elif (maxInd == 7):
		max_direction = 7*math.pi/8;

	#save in CSV
	local_energy_1D = local_energy.ravel();
	for val in local_energy_1D:
		array_csv.append(val);
		
	mean_amplitude_tab_1D = mean_amplitude_tab.ravel();
	for val in mean_amplitude_tab_1D:
		array_csv.append(val);
		
	local_energy_direction_1D = local_energy_direction.ravel();
	for val in local_energy_direction_1D:
		array_csv.append(val);

	array_csv.append(max_direction);
	
	return array_csv;