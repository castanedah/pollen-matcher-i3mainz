import cv2;
import particleProps as pp;
import numpy as np;
import sys;
import os,glob;
from threadER import ThreadManager

def getParticle(file,id):
	imgRGB = cv2.imread(file);
	img = cv2.imread(file,0);

	median = cv2.medianBlur(img,21);

	thres = pp.createThresold(median,0,255);

	thresRGB = pp.colorize(thres);
	#imgRGB = pp.applieMask(imgRGB,thresRGB);

	particle = pp.getParticle(imgRGB,id,file,thres);
	partProp = particle['propertys'];
	outTab = [];
	
	for i in range(0,len(partProp)):
		outTab.append(partProp[i]);
			
	return outTab;

def createCategorieTable(folders,picExtension):
	manager = ThreadManager();
	ret = [];
	type = 0;
	# Array of the normal type
	tabTest = [];
	for folder in folders:
		print folder;
	
		print 'Importing folder "'+folder+'"';
		dir = os.path.join(folder,"*."+picExtension);
		
		tabFichier = glob.glob(dir);
		index = 0;
		
		for file in tabFichier:
			tabTest.append(type);
			manager.addThread('particle '+str(index),getParticle,(tabFichier[index],index));
			index = index+1;
			
		res = manager.execute();
		
		for result in res:
			props = result;
			newPart = {'name':type,'propertys':props};
			ret.append(newPart);
			
		type = type+1;
		
	saveFile("normalResult.csv",tabTest,'\n');
	
	return ret;

def tableToCsv(table):
	
	file = [];
		
	for el in table:
	
		fileLine = str(el['name']);
		for prop in el['propertys']:
			fileLine = fileLine + ',' + str(prop);
			
		file.append(fileLine);
		
	return file;		

def saveFile(fName,array,newLine):
	file = '';
	for line in array:
		file = file + str(line) + newLine;
		
	f = open(fName, 'w');
	f.write(file);
	f.close();
	
def getFolders(path,signature):
	folder = [];
	for obj in os.listdir(path):
		if(obj.find('.') == -1 and obj.find(signature)==0):
			folder.append(obj);
	return folder;
	
#-------------------------------------------- MAIN
def main():
	folder = sys.argv[1];
	outCsv = sys.argv[2];
	
	folders = getFolders('.',',');
	
	print

	table = createCategorieTable(folders,'tif');

	print 'Saving csv table...';
	file = tableToCsv(table);
		
	saveFile(outCsv,file,'\n');
	
if(__name__ == '__main__'):
	main();

#-------------------------------------------- EXEC