import numpy as np;

#find all the peaks in a tables of values
def findPeaks(tab):
	peaksVal = [];
	peaksInd = [];
	meiVal = -1000;
	meiInd = -1000;
	valPrec = 0;
	asc = False;
	for ind,val in enumerate(tab):
		if (val >= meiVal):
			if(val < valPrec):
				asc = False;
			else:
				asc = True;
				
			meiVal = val;
			meiInd = ind;
			
		elif(asc==True):
			peaksVal.append(meiVal);
			peaksInd.append(meiInd);
			meiVal = -1000;
			meiInd = -1000;
			asc = False;
			
		else:
			asc = False;
			
		if(ind == len(tab)-1 and meiVal != -1000):
			peaksVal.append(meiVal);
			peaksInd.append(meiInd);
			
		valPrec = val;

	return (peaksInd,peaksVal);
	
	
def findPeaks2(tabVal,tabInd):
	peaksVal = [];
	peaksInd = [];
	meiVal = -1000;
	meiInd = -1000;
	valPrec = 0;
	asc = False;
	for ind,val in enumerate(tabVal):
		if (val >= meiVal):
			if(val < valPrec):
				asc = False;
			else:
				asc = True;
				
			meiVal = val;
			meiInd = ind;
			
		elif(asc==True):
			peaksVal.append(meiVal);
			peaksInd.append(tabInd[meiInd]);
			meiVal = -1000;
			meiInd = -1000;
			asc = False;
			
		else:
			asc = False;
			
		if(ind == len(tabVal)-1 and meiVal != -1000):
			peaksVal.append(meiVal);
			peaksInd.append(tabInd[meiInd]);
			
		valPrec = val;

	return (peaksInd,peaksVal);
	
#restrict the number of peaks, keep all the best values (with their index)
def restrictPeak(peaksInd,peaksVal,numberOfPeaks):
	res_peaksVal = [];
	res_peaksInd = [];
	
	traite_peaksInd = peaksInd[:];
	traite_peaksVal = peaksVal[:];
	
	if(numberOfPeaks > len(peaksVal)):
		print "WARNING : number of peaks wanted less than number of peaks given";
		numb = numberOfPeaks - len(peaksVal);
		for i in range(0,numb):
			peaksVal.append(-999);
			peaksInd.append(0);
			
	
	for i in range(0,numberOfPeaks):
		meiVal = -1000;
		meiInd = -1000;
		
		for ind,val in enumerate(traite_peaksVal):
			if (val > meiVal):
				meiVal = val;
				meiInd = ind;
				
		res_peaksVal.append(traite_peaksVal[meiInd]);
		res_peaksInd.append(traite_peaksInd[meiInd]);
		
		traite_peaksVal.remove(traite_peaksVal[meiInd]);
		traite_peaksInd.remove(traite_peaksInd[meiInd]);
		
	return (res_peaksInd,res_peaksVal);
	
#remove from the list given in parameter the lower values
def restrictPeakRemove(peaksInd,peaksVal,numberOfPeaks):
	res_peaksVal = [];
	res_peaksInd = [];
	
	traite_peaksInd = peaksInd[:];
	traite_peaksVal = peaksVal[:];
	
	if(numberOfPeaks > len(peaksVal)):
		raise Exception("number of peaks lower that the restriction. Please decrease the restriction value");
	
	for i in range(0,len(peaksInd)-numberOfPeaks):
		minVal = 1000;
		minInd = 1000;
		
		for ind,val in enumerate(traite_peaksVal):
			if (val < minVal):
				minVal = val;
				minInd = ind;
		
		traite_peaksVal.remove(traite_peaksVal[minInd]);
		traite_peaksInd.remove(traite_peaksInd[minInd]);
		
	return (traite_peaksInd,traite_peaksVal);
	
#return a list of the difference between the indexes
def tabDifference(indPeaks):
	tab_res = [];
	for i in range(0,len(indPeaks)-1):
		tab_res.append(abs(indPeaks[i+1]-indPeaks[i]));
		
	return tab_res;
	
#return the mean of a table
def meanDistance(tab):
	return np.mean(tab);
		
#return the variance of a table
def varianceDistance(tab,mean):
	res_var = 0;
	
	for i in tab:
		res_var = res_var + (i-mean)**2;
		
	res = res_var / len(tab);
	return res;
	
#sort the table of indices, and update the table of values
def sortIndices(tabInd, tabVal):
	size = len(tabInd);
	for i in range(size-1,-1,-1):
		for j in range(0,i):
			if(tabInd[j] > tabInd[j+1]):
				tmp = tabInd[j];
				tabInd[j] = tabInd[j+1];
				tabInd[j+1] = tmp;
				
				tmp = tabVal[j];
				tabVal[j] = tabVal[j+1];
				tabVal[j+1] = tmp;