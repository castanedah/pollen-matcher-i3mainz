#############################################
#											#
#	import csvManager as cm;				#
#											#
#	array = [];								#
#											#
#	array.append([1,2,3,5,6,7]);			#
#	array.append([1,2,3,5,6,7]);			#
#											#
#	cm.saveCSVfile2('test.csv',array);		#
#											#
#############################################

#------------------------------------------------ EASY SAVE
#save a 2D array easily
def saveCSVfile2(fName,array):
	saveCSVfile(fName,array,',','\n');
	
#save a 1D array easily
def saveCSVfile2_1D(fName,array):
	saveCSVfile_1D(fName,array,',','\n');

#------------------------------------------------ FULL SAVE
#save a 2D array with special characters
def saveCSVfile(fName,array,delimiter,newLine):
	file = '';
	for line in array:
		for case in line:
			file = file + str(case) + str(delimiter);
		file = file + newLine;
	f = open(fName, 'w');
	f.write(file);
	f.close();
	
#save a 1D array with special characters
def saveCSVfile_1D(fName,array,delimiter,newLine):
	file = '';
	for case in array:
		file = file + str(case) + str(delimiter);
		
	f = open(fName, 'w');
	f.write(file);
	f.close();