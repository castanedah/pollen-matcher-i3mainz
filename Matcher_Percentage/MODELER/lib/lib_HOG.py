import cv2
import os
import numpy as np

					
# calculate the histogram of oriented gradiants
# return a 1D matrix
def hog(img):
	bin_n = 16;
	gx = cv2.Sobel(img, cv2.CV_32F, 1, 0)
	gy = cv2.Sobel(img, cv2.CV_32F, 0, 1)
	mag, ang = cv2.cartToPolar(gx, gy)

	# quantizing binvalues in (0...16)
	bins = np.int32(bin_n*ang/(2*np.pi))

	# Divide to 4 sub-squares
	bin_cells = bins[:10,:10], bins[10:,:10], bins[:10,10:], bins[10:,10:]
	mag_cells = mag[:10,:10], mag[10:,:10], mag[:10,10:], mag[10:,10:]
	hists = [np.bincount(b.ravel(), m.ravel(), bin_n) for b, m in zip(bin_cells, mag_cells)]
	hist = np.hstack(hists)
	return hist
	
def main(file):
	img = cv2.imread(file,0)
	res=hog(img);
	return res;