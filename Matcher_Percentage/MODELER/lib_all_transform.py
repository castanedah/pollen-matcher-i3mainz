import cv2;
import numpy as np;
import sys;
import os,glob;
sys.path.append("lib");
import lib_GaborWavelets as GW;
import lib_FourierTransform as FT;
import lib_LBP as LBP;
import lib_glcm as GLCM;
import lib_HOG as HOG;
import lib_sfta as SFTA;
import multiprocessing;
import lib_Haralick as HAR;
sys.path.append("lib_apertures");
import exe_all_toDetect as apertures;

def main(file,nProcess):
	tab = [];
	tab1 = GW.main(file);
	tab2 = FT.main(file);
	tab3 = LBP.main(file);
	tab4 = GLCM.main(file);
	tab5 = HOG.main(file);
	tab6 = HAR.main(file);
	tab7 = apertures.main(file,nProcess);
	
	for element in tab1:
		tab.append(element);

	for element in tab2:
		tab.append(element);
	
	for element in tab3:
		tab.append(element);
		
	for element in tab4:
		tab.append(element);
		
	for element in tab5:
		tab.append(element);
		
	for element in tab6:
		tab.append(element);
		
	for element in tab7:
		tab.append(element);
		
	return tab;