from threadER import ThreadManager;
import time;

def printer(pr1,pr2):
	time.sleep(5);
	print str(pr1)+' - '+str(pr2);
	return str(pr1)+' * '+str(pr2);

if(__name__ == '__main__'):
	manager = ThreadManager();
	#manager.forceCPU();

	manager.addThread('print 1',printer,('coucou1','caca1'));
	manager.addThread('print 2',printer,('coucou2','caca2'));
	manager.addThread('print 3',printer,('coucou3','caca3'));
	manager.addThread('print 4',printer,('coucou4','caca4'));
	manager.addThread('print 5',printer,('coucou5','caca5'));
	manager.addThread('print 6',printer,('coucou6','caca6'));
	manager.addThread('print 7',printer,('coucou7','caca7'));
	
	list = manager.execute();
	print list;

	raw_input();