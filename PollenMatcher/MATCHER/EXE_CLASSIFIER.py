#--------------------------------------------------------- IMPORT

import time;
import subprocess
import consoler as co;
import glob,os,shutil;
	
# -------------------------------------- GLOB VAR INIT
imgDir = 'toClass';

outCsv = 'csvOut.csv';

ModelCsv = '../model.csv';

sortedCsv = 'sorted.csv';
	
# -------------------------------------- MAIN EXECUTION

co.announce('CLASSIFICATION : STARTING....')

#--------------------------------------------------------- STARTING
co.step('Generating images attributes...');
#make a CSV file from all images in the folder 'toClass'
subprocess.call(["python", "_CLASSIFIER/csvGenerator.py",imgDir,outCsv]);

#pause between the two parts
time.sleep(2);

co.step('Matching model / attributes...');
#affected a class to the toClass files, from the model file, and write the result in console, and a csv file (scoreFile)
subprocess.call(["python", "_CLASSIFIER/csvMatcher.py",outCsv,ModelCsv,sortedCsv]);
#work done

#--------------------------------------------------------- ENDING
#shutil.rmtree('tmp');
co.announce('CLASSIFICATION : DONE');