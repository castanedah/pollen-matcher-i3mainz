import numpy as np;
from os.path import dirname;

fast = 'sorted.csv';
slow = 'sorted 1.csv';

total = 0;
error = 0;

fast = np.genfromtxt(open(fast), delimiter=',', dtype='f8')[:]
slow = np.genfromtxt(open(slow), delimiter=',', dtype='f8')[:]

for i in range(0,len(fast)):
	lineFast = fast[i];
	lineSlow = slow[i];
	attrFast = lineFast[0];
	attrSlow = lineSlow[0];
	printer = attrFast,'<->',attrSlow;
	total = total + 1;
	if(attrFast!=attrSlow):
		printer = str(printer)+' <--- ERROR';
		error = error + 1;
	print printer;
		
percent = (float(error)/float(total))*100;
print percent,'%';

raw_input();