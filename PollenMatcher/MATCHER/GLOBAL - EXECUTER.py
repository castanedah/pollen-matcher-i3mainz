#--------------------------------------------------------- IMPORT

import subprocess
import consoler as co;

#--------------------------------------------------------- MAINS EXECUTION

subprocess.call(["python", "EXE_SEGMENTER.py"]);
subprocess.call(["python", "EXE_CLASSIFIER.py"]);
subprocess.call(["python", "EXE_MOSIN.py"]);

#--------------------------------------------------------- ENDING

co.alert('Press Enter to continue...');
raw_input();