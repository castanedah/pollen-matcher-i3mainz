import cv2
import numpy as np;
import sys;
import pollenProp as pp;

if(len(sys.argv)>1):
	path = sys.argv[1];
else:
	path = '../Your images/4.tif'
	
image = cv2.imread(path);

tabPath = path.split('/');
path = tabPath[len(tabPath)-1];
tabPath = path.split('\\');
path = tabPath[len(tabPath)-1];

thr = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
thr = cv2.bitwise_not(thr);

#--------------------------------------------------
kernel = np.ones((5,5),np.uint8)
ero = 5
thr = cv2.medianBlur(thr,9);

ret,thr = cv2.threshold(thr,50,200,cv2.THRESH_BINARY)

thr = cv2.dilate(thr,kernel,iterations = ero)
thr = cv2.erode(thr,kernel,iterations = ero)

thr = cv2.distanceTransform(thr,cv2.cv.CV_DIST_L2,5)
cv2.normalize(thr, thr, 0, 1., cv2.NORM_MINMAX);
ret, thr = cv2.threshold(thr,0.2,255,0)
thr = np.uint8(thr)
thr = cv2.dilate(thr,kernel,iterations = ero)

thr = cv2.bitwise_not(thr);


print 'Segmenting image "'+path;
pollens,points = pp.getPollens(thr,image);

for pollen in pollens:
	props = pollen['propertys'];
	params = [];
	params.append({'name':'D','value':props[0]});
	params.append({'name':'GG','value':props[3]});
	params.append({'name':'R','value':props[4]});
	params.append({'name':'G','value':props[5]});
	params.append({'name':'B','value':props[6]});
		
	saveImg = pollen['image'];
	namePart = path+'_'+pollen['name'];
	
	accord = False;
	w,h,c = pollen['image'].shape;
	if(w>0 and h>0):
		accord = True;
	if(accord):
		print 'SAVING IMAGE -> '+namePart+'.tif';
		cv2.imwrite('toClass/'+namePart+'.tif',pollen['image']);

thr = cv2.drawKeypoints(thr, points, np.array([]), (255,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)