#-----------------------------------------------------------	IMPORTS

import cv2;
import numpy as np;
import subprocess;
import sys;
import os

#-----------------------------------------------------------	FUNCTIONS

#------------------------------***
def imgAverage(img):
			
	aB,aG,aR,alpha = cv2.mean(img);
	
	average = (aR+aG+aB)/3;
	return average;
#------------------------------***
def amountOfDifference(img):
	w,h,c = img.shape;
	amount = 0
	x1 = 0+amount;
	x2 = int(w/2);
	x3 = w-amount;
	
	y1 = 0+amount;
	y2 = int(h/2);
	y3 = h-amount;
	
	c1 = img[x1:x2 , y1:y2];c2 = img[x2:x3 , y1:y2];
	c3 = img[x1:x2 , y2:y3];c4 = img[x2:x3 , y2:y3];
	
	a1 = imgAverage(c1);a2 = imgAverage(c2);
	a3 = imgAverage(c3);a4 = imgAverage(c4);
	
	difference = abs(abs(a1-a2)-abs(a2-a3));
	return difference;
#------------------------------***
def isClear(path):
	img = cv2.imread(path);
	dif = amountOfDifference(img);
	if(dif>5):
		ret = True;
	else:
		ret = False;
		
	return not ret;
	
#-----------------------------------------------------------	MAIN
	
# --- getting path
if(len(sys.argv)>1):
	path = sys.argv[1];
else:
	path = 'gra.tif'
	
# --- checking particle clearness
clear = isClear(path);
if(clear):
	print ' type : CLEAR';
	algo = 'clear_blober.py';
else:
	print ' type : NOT_CLEAR';
	algo = 'unclear_blober.py';
	
#-----------------------------------------------------------	END
algo = '_SEGMENTER/'+algo;
subprocess.call(["python", algo,path]);
