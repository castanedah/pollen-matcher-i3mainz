#-----------------------------------------------------------	IMPORTS

import cv2
import numpy as np;
import sys;
import pollenProp as pp;
	
#-----------------------------------------------------------	MAIN

if(len(sys.argv)>1):
	path = sys.argv[1];
else:
	path = 'microGRA/4.tif'
	
print path

	
image = cv2.imread(path);

gray = cv2.imread(path,0);

tabPath = path.split('/');
path = tabPath[len(tabPath)-1];
tabPath = path.split('\\');
path = tabPath[len(tabPath)-1];

#------------------------------	IMAGE PROCESSING

gray_blur = cv2.GaussianBlur(gray, (5, 5), 0)
thresh = cv2.adaptiveThreshold(gray_blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
cv2.THRESH_BINARY_INV, 11, 1)

kernel = np.ones((3, 3), np.uint8)
closing = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE,kernel, iterations=4)

ero = 15
closing = cv2.dilate(closing,kernel,iterations = ero)
closing = cv2.erode(closing,kernel,iterations = ero)
closing = cv2.erode(closing,kernel,iterations = ero)
closing = cv2.dilate(closing,kernel,iterations = ero)
closing = cv2.bitwise_not(closing);

thr = closing.copy()

#------------------------------	POLLEN DETECTIONS

pollens,points = pp.getPollens(thr,image);

#------------------------------	SAVING POLLEN

for pollen in pollens:
	props = pollen['propertys'];
	params = [];
	saveImg = pollen['image'];
	namePart = path+'_'+pollen['name'];
	for param in params:
		namePart = namePart+'+'+param['name']+'='+str(int(round(param['value'])));
	#cv2.imshow(pollen['name'],pollen['image']);
	accord = False;
	w,h,c = pollen['image'].shape;
	if(w>0 and h>0):
		accord = True;
	print namePart,accord;
	if(accord):
		cv2.imwrite('toClass/'+namePart+'.tif',pollen['image']);