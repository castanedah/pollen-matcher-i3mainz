#-----------------------------------------------------------	IMPORTS

import cv2;
import numpy as np;
import subprocess;
import sys;
import os,glob;
import pollenProp as pp;

#-----------------------------------------------------------	FUNCTIONS

def unGluer(path):
	# load imag color and gray
	image = cv2.imread(path);
	thr = cv2.imread(path,0);
	
	# Filter image to geat a good threshold
	thr = cv2.medianBlur(thr,21);
	kernel = np.ones((6,6),np.uint8);
	ero = 3
	ret,thr = cv2.threshold(thr,0,255,cv2.THRESH_BINARY | cv2.cv.CV_THRESH_OTSU);
	thr = cv2.bitwise_not(thr);
	thr = cv2.distanceTransform(thr,cv2.cv.CV_DIST_L2,5)
	cv2.normalize(thr, thr, 0, 0.3, cv2.NORM_MINMAX);
	ret, thr = cv2.threshold(thr,0.2,255,0)
	thr = np.uint8(thr);
	thr = cv2.dilate(thr,kernel,iterations = 8);
	thr = cv2.bitwise_not(thr);
	
	# Get pollens particle from image
	pollens,points = pp.getPollens(thr,image);

	# if we see more than one pollen it means that the image is a "glued-pollen" image
	if(len(pollens)>1):
		remove = False;
	# --- For each pollen particle founded --- #
		for pollen in pollens:
			w,h,c = pollen['image'].shape;
		#	Check if the image is size conform
			remove = (w>10 and h>10)
			if(remove):
			# Save the particles images and remove de source
				imagePath = path+pollen['name']+".tif";
				cv2.imwrite(imagePath,pollen['image']);
				print "    Making - "+imagePath;
		if(remove):
			os.remove(path);

#-----------------------------------------------------------	FUNCTIONS

# Create the folder
image_dir = 'toClass/';
imageDir = os.path.join(image_dir,"*.tif");
files = glob.glob(imageDir);

# --- For each file in the folder specified --- #
for file in files:
	unGluer(file);