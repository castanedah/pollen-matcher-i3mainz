#-----------------------------------------------------------	IMPORTS

import cv2
import numpy as np;
import sys;
import pollenProp as pp;
	
#-----------------------------------------------------------	MAIN

if(len(sys.argv)>1):
	path = sys.argv[1];
else:
	path = 'microCLA/4.tif'
	
image = cv2.imread(path);
tabPath = path.split('/');
path = tabPath[len(tabPath)-1];
tabPath = path.split('\\');
path = tabPath[len(tabPath)-1];

thrF = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
thrF = cv2.bitwise_not(thrF);

#------------------------------	IMAGE PROCESSING

kernel = np.ones((5,5),np.uint8)
ero = 5
thrF = cv2.medianBlur(thrF,9);

ret,thrF = cv2.threshold(thrF,50,200,cv2.THRESH_BINARY)

thrF = cv2.dilate(thrF,kernel,iterations = ero)
thrF = cv2.erode(thrF,kernel,iterations = ero)

thrF = cv2.distanceTransform(thrF,cv2.cv.CV_DIST_L2,5)
cv2.normalize(thrF, thrF, 0, 1., cv2.NORM_MINMAX);
ret, thrF = cv2.threshold(thrF,0.2,255,0)
thrF = np.uint8(thrF)
thrF = cv2.dilate(thrF,kernel,iterations = ero)

thrF = cv2.distanceTransform(thrF,cv2.cv.CV_DIST_L2,5)
cv2.normalize(thrF, thrF, 0, 1., cv2.NORM_MINMAX);
ret, thrF = cv2.threshold(thrF,0.2,255,0)
thrF = np.uint8(thrF)
thrF = cv2.dilate(thrF,kernel,iterations = ero)

thrF = cv2.bitwise_not(thrF);

#------------------------------	COLOR PROCESSING

thrC = cv2.medianBlur(image,21);

R = 137
G = 137
B = 166
Rimg = thrC[:,:,2];
Gimg = thrC[:,:,1];
Bimg = thrC[:,:,0];
pas = -20;

ret,Rimg = cv2.threshold(Rimg,R-pas,R+pas,cv2.THRESH_BINARY);
ret,Bimg = cv2.threshold(Bimg,B-pas,B+pas,cv2.THRESH_BINARY);
thrC = cv2.add(Rimg,Bimg);
ret,thrC = cv2.threshold(thrC,0,255,cv2.THRESH_OTSU);
thrC = cv2.bitwise_not(thrC);

ero = 5
thrC = cv2.dilate(thrC,kernel,iterations = ero)
thrC = cv2.erode(thrC,kernel,iterations = ero*2)
thrC = cv2.dilate(thrC,kernel,iterations = ero)

thrC = cv2.bitwise_not(thrC);

thrF = cv2.bitwise_not(thrF);
thrC = cv2.bitwise_not(thrC);
thr = cv2.add(thrC,thrF);
thr = cv2.bitwise_not(thr);

#------------------------------	POLLEN DETECTIONS

pollens,points = pp.getPollens(thr,image);

#------------------------------	SAVING POLLEN

for pollen in pollens:
	props = pollen['propertys'];
	params = [];
	saveImg = pollen['image'];
	namePart = path+'_'+pollen['name'];
	for param in params:
		namePart = namePart+'+'+param['name']+'='+str(int(round(param['value'])));

	accord = False;
	w,h,c = pollen['image'].shape;
	if(w>0 and h>0):
		accord = True;
	print namePart,accord;
	if(accord):
		cv2.imwrite('toClass/'+namePart+'.tif',pollen['image']);