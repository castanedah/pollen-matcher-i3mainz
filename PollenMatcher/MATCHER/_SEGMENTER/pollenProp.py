#--------------------------------------------------------- IMPORT

import cv2
import numpy as np;
import sys;

#--------------------------------------------------------- FUNCTIONS
# GET.POLLEN----------------------------***
def getPollens(trait,image):
	keyPoints = pollenPoints(trait);
	
	pollens = [];
	
	# init index on first particle name
	index = 0;
	
	# --- for every blob in the blob detector --- #
	for point in keyPoints:
	# 	create a new pollen object
		newPollen = pollenProp(point,'particle_'+str(index),image);
		pollens.append(newPollen);
		
	#	increase index name
		index = index+1;
	return pollens,keyPoints;

# GET.POLLEN----------------------------***
# POLLEN.POINT--------------------------***
def pollenPoints(image):

	# create parameters for bob detection
	params = cv2.SimpleBlobDetector_Params()
	'''
	params.minThreshold = 0;
	params.maxThreshold = 255;
	'''
	params.filterByArea = True
	params.minArea = 300
	params.maxArea = 1000000
	params.filterByCircularity = True
	params.minCircularity = 0.2
	params.filterByConvexity = True
	params.minConvexity = 0.0
	params.filterByInertia = True
	params.minInertiaRatio = 0.25
	ver = (cv2.__version__).split('.')
	
	# create the detector
	if int(ver[0]) < 3 :
		detector = cv2.SimpleBlobDetector(params)
	else : 
		detector = cv2.SimpleBlobDetector_create(params)
	
	# get blobs from the image by the detector
	keypoints = detector.detect(image)
	return keypoints;

# POLLEN.POINT--------------------------***
# POLLEN.PROP---------------------------***
def pollenProp(point,name,image):
	pollenImage = getPointImage(point,image);
	
	colorImage = getColorImage(point,image);
	
	r,g,b = getColorsAverage(colorImage);
	gray = (r+g+b)/3;
	
	propertys = [];
	propertys.append(point.size);
	propertys.append(point.pt[0]);
	propertys.append(point.pt[1]);
	propertys.append(gray);
	propertys.append(r);
	propertys.append(g);
	propertys.append(b);
	
	newPollen = {'name':name,'image':pollenImage,'propertys':propertys};
	return newPollen;

# POLLEN.PROP---------------------------***
# GET.POINT.IMAGE-----------------------***
def getPointImage(point,im):
	diam = point.size;
	diam2 = diam+30;
	x,y = point.pt;
	x1 = round(x-diam2);
	y1 = round(y-diam2);
	x2 = round(x+diam2);
	y2 = round(y+diam2);
	pollenImage = im[y1:y2,x1:x2];
	return pollenImage;

# GET.POINT.IMAGE-----------------------***
# GET.COLOR.IMAGE-----------------------***
def getColorImage(point,im):
	diam = point.size;
	diam2 = diam/3;
	x,y = point.pt;
	x1 = round(x-diam2);
	y1 = round(y-diam2);
	x2 = round(x+diam2);
	y2 = round(y+diam2);
	pollenImage = im[y1:y2,x1:x2];
	return pollenImage;

# GET.COLOR.IMAGE-----------------------***
# GET.COLOR.AVERAGE.SINGLE.CHANNEL------***
def getColorAverageSingleChannel(img):
	b,g,r,a = cv2.mean(img);
	return (r+g+b)/3;

# GET.COLOR.AVERAGE.SINGLE.CHANNEL------***
# GET.COLORS.AVERAGE--------------------***
def getColorsAverage(img):
	b,g,r,a = cv2.mean(img);
	return r,g,b;

# GET.COLORS.AVERAGE--------------------***