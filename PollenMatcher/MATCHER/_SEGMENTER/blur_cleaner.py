#-----------------------------------------------------------	IMPORTS

import cv2;
import numpy as np;
import subprocess;
import sys;
import os,glob;

#-----------------------------------------------------------	FUNCTIONS

#------------------------------***
def amountOfBlur(img):
	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	w,h = gray.shape;
	amount = 20;
	crop = gray[amount:(w-amount),amount:(h-amount)];
	blur = np.max(cv2.convertScaleAbs(cv2.Laplacian(crop,3)))
	return blur;
#------------------------------***
def isBlured(path):
	img = cv2.imread(path);
	img = cv2.medianBlur(img,11);
	blur = amountOfBlur(img);
	if(blur<30):
		ret = True;
	else:
		ret = False;
		
	return ret;

#------------------------------***
def blurClear(path):
	blured = isBlured(path);
	if(blured):
		os.remove(path);
		print path+" [Blurred] -> removed";
#-----------------------------------------------------------	MAIN

image_dir = 'toClass/';

imageDir = os.path.join(image_dir,"*.tif");

for file in glob.glob(imageDir):
	blurClear(file);