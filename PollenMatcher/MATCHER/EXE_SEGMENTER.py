#--------------------------------------------------------- IMPORT

import subprocess
import consoler as co;
import glob,os;

#--------------------------------------------------------- GLOBAL VARIABLE INIT

image_dir = 'Your images/';
imageDir = os.path.join(image_dir,"*.tif");

#--------------------------------------------------------- MAINS EXECUTION

co.announce('IMAGE SEGMENTATION : STARTING...')

#--------------------------------------------------------- STARTING

# get blob from every image in the image_dir directory
for file in glob.glob(imageDir):
	co.step('file "'+file+'"');
	subprocess.call(["python", "_SEGMENTER/type_checker.py",file]);

co.step('Un Gluer...');
subprocess.call(["python", "_SEGMENTER/un_gluer.py"]);

co.step('Blur image remover...');
subprocess.call(["python", "_SEGMENTER/blur_cleaner.py"]);
	
#--------------------------------------------------------- ENDING
co.announce('SEGMENTATION : DONE')
