#--------------------------------------------------------- IMPORT

import time;
import subprocess
import consoler as co;
import glob,os,shutil;
	
# -------------------------------------- GLOB VAR INIT
sorted = 'sorted.csv';
mosin = '../mosin.csv';
toClass = 'toClass/';

	
# -------------------------------------- MAIN EXECUTION

co.announce('MOSIN TRANSFORM : STARTING....')

#--------------------------------------------------------- STARTING
co.step('Transforming image name with types...');
time.sleep(2);
#make a CSV file from all images in the folder 'toClass'
subprocess.call(["python", "_CLASSIFIER/mosinTransform.py",sorted,mosin,toClass]);

#--------------------------------------------------------- ENDING
#shutil.rmtree('tmp');
co.announce('MOSIN : DONE');