import cv2
import numpy as np
import scipy as sc
from scipy import ndimage
from skimage import io
from matplotlib import pyplot as plt
import peak as pk
import csvManager as cm

def main(file):
	#loading
	img = cv2.imread(file,0)
	#fft transform 
	f = np.fft.fft2(img)
	fshift = np.fft.fftshift(f)
	magnitude_spectrum = 20*np.log(np.abs(fshift))

	img2 = img.ravel();
	fshift2 = fshift.ravel();

	#Magnitude histogram
	plt.hist(magnitude_spectrum.ravel(),256,[0,256],normed=True);
	plt.title('Magnitude histogram')

	#try to make a plot of frequencies

	freq = np.fft.fftfreq(img2.shape[-1]) # time sloth of histogram is 1 hour
	line2d = plt.plot(freq, np.log10(np.abs(fshift2))**2);

	#array to put in csv
	array_csv = [];

	#Extract values
	xvalues = line2d[0].get_xdata();
	yvalues = line2d[0].get_ydata();
	
	plt.clf();
	plt.cla();
	
	#find the peaks
	(tabInd,tabVal) = pk.findPeaks2(yvalues,xvalues);

	#number of peaks restriction
	restr = 10;

	(tabInd2,tabVal2) = pk.restrictPeakRemove(tabInd,tabVal,restr);

	pk.sortIndices(tabInd2,tabVal2);

	#difference table
	tab_diff = pk.tabDifference(tabInd2);

	array_csv = tab_diff[:];
	
	#computes
	mean = pk.meanDistance(tab_diff);
	
	variance = pk.varianceDistance(tab_diff,mean);
	
	array_csv.append(mean);
	array_csv.append(variance);
	
	return array_csv