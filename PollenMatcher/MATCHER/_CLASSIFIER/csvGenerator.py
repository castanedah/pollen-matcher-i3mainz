import cv2;
import particleProps as pp;
import numpy as np;
import sys;
import os,glob;
from threadER import ThreadManager

def getParticle(file,id):
	imgRGB = cv2.imread(file);
	img = cv2.imread(file,0);

	median = cv2.medianBlur(img,21);

	thres = pp.createThresold(median,0,255);

	thresRGB = pp.colorize(thres);
	#imgRGB = pp.applieMask(imgRGB,thresRGB);

	particle = pp.getParticle(imgRGB,id,file,thres);
	partProp = particle['propertys'];
	outTab = [];
	
	for i in range(0,len(partProp)):
		outTab.append(partProp[i]);
			
	return outTab;

def createCategorieTable(folders,picExtension):
	manager = ThreadManager();
	ret = [];
	type = 0;
	for folder in folders:
	
		print 'Importing folder "'+folder+'"';
		dir = os.path.join(folder,"*."+picExtension);
		
		tabFichier = glob.glob(dir);
		index = 0;
		
		for file in tabFichier:
			manager.addThread('particle '+str(index),getParticle,(tabFichier[index],index));
			index = index+1;
			
		res = manager.execute();
		
		for result in res:
			props = result;
			newPart = {'name':type,'propertys':props};
			ret.append(newPart);
			
		type = type+1;
	
	return ret;
	
def createCategorieTableNOMULTI(folders,picExtension):
	ret = [];
	type = 0;
	for folder in folders:
	
		print 'Importing folder "'+folder+'"';
		dir = os.path.join(folder,"*."+picExtension);
		
		tabFichier = glob.glob(dir);
		index = 0;
		
		for file in tabFichier:
			print 'image ['+str(index+1)+'/'+str(len(tabFichier))+']';
			props = getParticle(file,index);
			newPart = {'name':type,'propertys':props};
			ret.append(newPart);
			index = index+1;
	
	return ret;

def tableToCsv(table):
	
	file = [];
		
	for el in table:
	
		fileLine = str(el['name']);
		for prop in el['propertys']:
			fileLine = fileLine + ',' + str(prop);
			
		file.append(fileLine);
		
	return file;		

def saveFile(fName,array,newLine):
	file = '';
	for line in array:
		file = file + line + newLine;
		
	f = open(fName, 'w');
	f.write(file);
	f.close();
	
def commandGetter(commandFile):
	mtp = open(commandFile);
	command = mtp.read();
	properties = command.split('\n');
	
	propertieObjs = [];
	
	for parts in properties:
		command = command.replace(' ','');
		name = command.split(':')[0];
		value = command.split(':')[1];
		newProp = {'name':name,'value':(value == 'true' or value == 'True')};
		propertieObjs.append(newProp);
	
	return propertieObjs;
	
#-------------------------------------------- MAIN
def main():
	folder = sys.argv[1];
	outCsv = sys.argv[2];
	
	multithreadingProps = '../multi-threading.properties';
	enableMultiThreading = commandGetter(multithreadingProps)[0]['value'];

	if(enableMultiThreading):
		print 'MULTI THREADING ENABLED';
		table = createCategorieTable([folder],'tif');
	else:	
		print 'MULTI THREADING DISABLED';
		table = createCategorieTableNOMULTI([folder],'tif');

	print 'Saving csv table...';
	file = tableToCsv(table);
		
	saveFile(outCsv,file,'\n');
	
if(__name__ == '__main__'):
	main();

#-------------------------------------------- EXEC
	
