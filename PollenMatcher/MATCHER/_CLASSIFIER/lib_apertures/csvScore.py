import numpy as np;
import os, glob;
import math;
from numpy import savetxt;
from sklearn import cross_validation, svm, neighbors;
from sklearn.ensemble import RandomForestClassifier;
from sklearn.naive_bayes import GaussianNB;
from matplotlib import pyplot as plt;
import cv2;

#read in data, parse into training and target sets
# although using "all_better2.csv" works fine: gives results of 100% due to distinguishable background
# bool : 0 if we want to do the restriction, 1 else
def main(toClassFile,modelFile,type,bool,nProcess):

	modelFile = "_CLASSIFIER/lib_apertures/"+modelFile;
	toClassFile = "_CLASSIFIER/lib_apertures/"+toClassFile;
	#loading data : the file to sort first, and the model then
	dataset_toClass = np.genfromtxt(open(toClassFile), delimiter=',', dtype='f8')[:]
	#number of columns (CAUTION)
	if(len(dataset_toClass)==105):
		data_toClass = np.array(dataset_toClass[1:]);
	else:
		data_toClass = np.array([x[1:] for x in dataset_toClass]);
	
	dataset_model = np.genfromtxt(open(modelFile), delimiter=',', dtype='f8')[1:]
	target = np.array([x[0] for x in dataset_model]);
	train = np.array([x[1:] for x in dataset_model]);

	#define X & y, like in lessons
	X,y = train, target

	#use support vector machines classifier
	#use linear, because other are too slow
	clf = svm.SVC(kernel='linear', C=1)
	
	# total length of the train
	n_sample = len(X)

	# permutation, because classes are in good order
	np.random.seed(0)
	order = np.random.permutation(n_sample)
	X = X[order]
	y = y[order].astype(np.float)

	# define X_train and y_train, respectively train and target
	X_train = X[:]
	y_train = y[:]
	# define the test : so the class table
	X_test = data_toClass[:]
	
	# define the model
	# print "   Fit begin"
	clf = clf.fit(X_train,y_train)
	# print "   Fit end"
	# predict, and affect a class value to the test data
	resA = clf.predict(X_test);
	
	#different execution, see boolean given
	if not (bool):
		tabStr = ["Alder","Birch","Hazel","Mugwort"];
		tabTypes = [];
		for element in resA:
			element = int(element);
			tabTypes.append(tabStr[element]);
		return tabTypes;
	else:
		tabImg = createCategorieTable(["tmp/temp"+str(nProcess)],"jpg");
		
		tabIndex = [];
		
		#number in the tab (index)
		imgNumber = 0;
		
		#modify the toClassFile (not directly, store into another file), to add the classes
		for i in range(0,len(resA)):
			dataset_toClass[i][0] = int(resA[i]);
			
			#it's an aperture !
			if(int(resA[i]) == 1):
				tabIndex.append(imgNumber);

			imgNumber = imgNumber + 1;
			
		max = int(math.sqrt(imgNumber));
		
		completeTable = [];
		
		#for all the images
		for number in tabIndex:
			# i : division by the max
			i = int(math.floor(number/max));
			# j : modulo by the max
			j = int(number % max);
			completeTable.append((i,j));
			
		# print completeTable;
			
		tabFinal = findGroup(completeTable,max,type);
		
		cpt = 0;
		
		#display the images selected
		for number in tabFinal:
			cpt = cpt + 1;
			path = tabImg[number];
			img = cv2.imread(path);
			cv2.imwrite("resultApertures/apertures"+str(nProcess)+"/img"+str(number)+".jpg",img);
			
		#scores = cross_validation.cross_val_score(clf, X, y, cv=6, scoring='f1_weighted');
		return cpt;
	
#make the euclidian distance, return true if lower than 3, else true
def euclidian(i1,i2,j1,j2,type):
	if (type == "Alder"):
		restr = 3;
	else:
		restr = 3;
	return (abs(i1-i2)+abs(j1-j2)) < restr;
	
# detect the maximum value of the group -> the number of group
def maxGroup(tabGroup):
	max = 0;
	for element in tabGroup:
		if(element[2] > max):
			max = element[2];
			
	return max;
	
#affect group of same aperture
def findGroup(tabComp,max,type):
	#store the tab with the i, the j, and the group
	tabGroup = [];
	for (i,j) in tabComp:
		tabGroup.append([i,j,0]);
	
	#active group : if we need another
	activeGroup = 1;
	
	#for all the values
	for cpt in range(0,len(tabGroup)):
		#we take the i and the j of the image
		i1 = tabGroup[cpt][0];
		j1 = tabGroup[cpt][1];
		inGroup = False;
		
		#for all the values previous the current:
		for cpt2 in range(0,cpt):
			#we take the i and the j of the image
			i2 = tabGroup[cpt2][0];
			j2 = tabGroup[cpt2][1];
			
			#if the euclidian compute is lower than 3, we affect to the current image the same group than the image that is compared
			if(euclidian(i1,i2,j1,j2,type)):
				tabGroup[cpt][2] = tabGroup[cpt2][2];
				inGroup = True;
				break;
		
		#if no one is close to this, we affect another group.
		if (inGroup == False):
			tabGroup[cpt][2] = activeGroup;
			activeGroup = activeGroup + 1;
	
	#for all the values
	for cpt in range(len(tabGroup)-1,-1,-1):
		#we take the i and the j of the image
		i1 = tabGroup[cpt][0];
		j1 = tabGroup[cpt][1];
		inGroup = False;
		
		#for all the values previous the current:
		for cpt2 in range(len(tabGroup)-1,cpt,-1):
			#we take the i and the j of the image
			i2 = tabGroup[cpt2][0];
			j2 = tabGroup[cpt2][1];
			
			#if the euclidian compute is lower than 3, we affect to the current image the same group than the image that is compared
			if(euclidian(i1,i2,j1,j2,type)):
				tabGroup[cpt][2] = tabGroup[cpt2][2];
				inGroup = True;
				break;
	
	#array that will be returned at the end : all the mean pictures
	tabImg = [];
	
	#for all the values of each group
	for i in range(1,maxGroup(tabGroup)+1):
		iSum = 0;
		jSum = 0;
		nbElem = 0;
		# for all element : what group ?
		for element in tabGroup:
			# if current group, we add the i and the j to the others i and j as the same group
			if (element[2] == i):
				iSum = iSum + element[0];
				jSum = jSum + element[1];
				nbElem = nbElem + 1;
				
		#only if the number of element is not null
		if (nbElem != 0):	
			#mean of i and j
			iMoy = int(round(iSum/nbElem));
			jMoy = int(round(jSum/nbElem));
			
			# get an number of image, that we will take
			moy = iMoy * max + jMoy;
			
			#new image selection
			tabImg.append(moy);
		
	return tabImg;
	
# Take all the file in a repository
def createCategorieTable(folders,picExtension):
	ret = [];
	for folder in folders:
		dir = os.path.join(folder,"*."+picExtension);
		
		for file in glob.glob(dir):		
			ret.append(file);
		
	return ret;