import multiprocessing;

class ThreadManager(object):
	def __init__(self):
		print '   ThreadLanager initialisation...'
		
		manager = multiprocessing.Manager();
		
		self.threadList = [];
		self.coresCount = multiprocessing.cpu_count();
		print '   ThreadLanager initialisation DONE';
	
	def addThread(self,name,func,args):
		newThread = Thread(name,func,args);
		self.threadList.append(newThread);
		print '   "'+name+'" added to queue';
		
	def printStart(self,processName):
		trait = '   ----------';
		for i in range(0,len(processName)):
			trait = trait + '-';
		trait = trait + '--';	
		print trait;
		print '   * exec -> '+ processName+' *';	
		print trait;
		
	def forceCPU(self,nbCpu):
		cpuStr = ('cpu', 'cpus')[nbCpu > 1];
		print '   CPU forced at',nbCpu,cpuStr;
		nbCpu = nbCpu+1;
		self.coresCount = nbCpu;
		
	def execute(self):
		coreAllowed = self.coresCount-1;
		processIndex = 0;
		total = len(self.threadList);
		returnList = [];
		
		while(processIndex < total):
			processCount = 0;
			tab_process = [];
			poolNumber = (coreAllowed, (total-processIndex))[(total-processIndex) < coreAllowed];
			pool = multiprocessing.Pool(processes=poolNumber);
			
			while(processCount < coreAllowed):
				if(processIndex < total):
					
					process = self.threadList[processIndex];
					self.printStart(process.name);
					
					tab_process.append(pool.apply_async(process.function, process.arguments));    # evaluate "f(10)" asynchronously
					
			
				processIndex = processIndex + 1;
				processCount = processCount + 1;
				
				
			print '   Running...';
			pool.close();
			pool.join();
			print '   Run done';
			
			for process in tab_process:
				returnList.append(process.get());
				
		self.threadList = [];
		return returnList;

class Thread():
	def __init__(self,name,func,args):
		self.function = func;
		self.name = str(name);
		self.arguments = args;