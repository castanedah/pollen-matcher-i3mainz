import numpy as np
import os
import cv2
import csv
from skimage.feature import greycomatrix, greycoprops
from skimage import data
from matplotlib import pyplot as plt
import skimage.io
import skimage.feature
import csvManager

# @image_input : string, is the name of the image 
# return a matrix which contain information about the contrast, the energy, the correlation and the asm of the image
def glcm (image_input) :

   im=cv2.imread(image_input,0)
   im = skimage.img_as_ubyte(im)
   im /= 32
   g = skimage.feature.greycomatrix(im, [1], [0], levels=256, symmetric=False, normed=True)

   contrast =    [skimage.feature.greycoprops(g, 'contrast')[0][0]]
   energy =      [skimage.feature.greycoprops(g, 'energy')[0][0]]
   homogenity =  [skimage.feature.greycoprops(g, 'homogeneity')[0][0]]
   correlation = [skimage.feature.greycoprops(g, 'correlation')[0][0]]
   asm =         [skimage.feature.greycoprops(g, 'ASM')[0][0]]

   glcm = contrast + energy + homogenity + correlation 
   return glcm

def main(file):
	img=glcm(file);
	return img;