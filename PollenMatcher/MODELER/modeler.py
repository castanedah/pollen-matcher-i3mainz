import cv2;
import particleProps as pp;
import os,glob,shutil;
import sys;
from threadER import ThreadManager;

def getParticle(file,id):
	imgRGB = cv2.imread(file);
	img = cv2.imread(file,0);

	median = cv2.medianBlur(img,21);

	thres = pp.createThresold(median,0,255);

	thresRGB = pp.colorize(thres);
	#imgRGB = pp.applieMask(imgRGB,thresRGB);

	particle = pp.getParticle(imgRGB,id,file,thres);
	partProp = particle['propertys'];
	outTab = [];
	
	for i in range(0,len(partProp)):
		outTab.append(partProp[i]);
			
	return outTab;

def createCategorieTable(folders,picExtension):
	manager = ThreadManager();
	ret = [];
	type = 0;
	
	ress = []
	typeList = [];
	indexEnTout = 0;
	
	for folder in folders:
	
		print 'Importing folder "'+folder+'"';
		dir = os.path.join(folder,"*."+picExtension);
		
		tabFichier = glob.glob(dir);
		index = 0;
		
		for file in tabFichier:
			typeList.append(type);
			manager.addThread('particle type ['+str(type)+'] image ['+str(index+1)+'/'+str(len(tabFichier))+'] total ['+str(indexEnTout)+']',getParticle,(tabFichier[index],indexEnTout));
			index = index+1;
			indexEnTout = indexEnTout+1;
			
		type = type+1;
			
	res = manager.execute();
	
	for i in range(0,len(res)):
		type = typeList[i];
		props = res[i];
		newPart = {'name':type,'propertys':props};
		ret.append(newPart);
		
	print "done";
	return ret;
	
def createCategorieTableNOMULTI(folders,picExtension):
	ret = [];
	type = 0;
	indexEnTout = 0;
	for folder in folders:
	
		print 'Importing folder "'+folder+'"';
		dir = os.path.join(folder,"*."+picExtension);
		
		tabFichier = glob.glob(dir);
		index = 0;
		
		for file in tabFichier:
			print 'particle type ['+str(type)+'] image ['+str(index+1)+'/'+str(len(tabFichier))+'] total ['+str(indexEnTout)+']'
			props = getParticle(file,indexEnTout);
			newPart = {'name':type,'propertys':props};
			ret.append(newPart);
			indexEnTout = indexEnTout+1;
			
		type = type+1;
			
	print "done";
	return ret;
	
def tableToCsv(table,folderTab):
	
	file = [];
	for el in table:
	
		fileLine = str(el['name']);
		for prop in el['propertys']:
			fileLine = fileLine + ',' + str(prop);
			
		file.append(fileLine);
		
	mosin = [];
	
	index = 0;
	for folder in folderTab:
		mosin.append(str(index)+','+folder);
		index = index + 1;
		
	return file,mosin;		

def saveFile(fName,array,newLine):
	file = '';
	for line in array:
		file = file + line + newLine;
		
	f = open(fName, 'w');
	f.write(file);
	f.close();
	
def commandGetter(commandFile):
	mtp = open(commandFile);
	command = mtp.read();
	properties = command.split('\n');
	
	propertieObjs = [];
	
	for parts in properties:
		command = command.replace(' ','');
		name = command.split(':')[0];
		value = command.split(':')[1];
		newProp = {'name':name,'value':(value == 'true' or value == 'True')};
		propertieObjs.append(newProp);
	
	return propertieObjs;
	
def getFolders(path):
	folder = [];
	for obj in os.listdir(path):
		if(obj.find('.') == -1 and obj.find('_')==0):
			folder.append(obj);
	return folder;

#-------------------------------------------- MAIN

def main():
	outFile = '../model.csv';
	mosinFile = '../mosin.csv';
	multithreadingProps = '../multi-threading.properties';
	folderTab = getFolders('.');
	
	enableMultiThreading = commandGetter(multithreadingProps)[0]['value'];
	
	if(enableMultiThreading):
		print 'MULTI THREADING ENABLED';
		table = createCategorieTable(folderTab,'tif');
	else:	
		print 'MULTI THREADING DISABLED';
		table = createCategorieTableNOMULTI(folderTab,'tif');
		
	file,mosin = tableToCsv(table,folderTab);

	saveFile(outFile,file,'\n');
	saveFile(mosinFile,mosin,'\n');

if __name__ == '__main__':
	main();

#-------------------------------------------- EXEC
	
