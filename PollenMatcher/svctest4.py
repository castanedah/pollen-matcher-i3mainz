import numpy as np
from numpy import savetxt
from sklearn import cross_validation, svm, neighbors
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB

#read in data, parse into training and target sets
# although using "all_better2.csv" works fine: gives results of 100% due to distinguishable background

dataset = np.genfromtxt(open('model.csv','r'), delimiter=',', dtype='f8')[1:]

target = np.array([x[0] for x in dataset])
train = np.array([x[1:] for x in dataset])

X,y = train, target

#use support vector machines classifier
# cross validation -- in scikit learn, cross_validation defaults to k-folds. cv = number of folds used
# for svm, linear was the most successful kernal tested: poly had same results but took 612 s. RBF had only %49, sigmoid 29%
# for svm, C value best at 1 or 20 (tested against 340,18, 1-10,20,30)
clf = svm.SVC(kernel='linear', C=1)
#clf = RandomForestClassifier(n_estimators=100)
#clf = GaussianNB()
#n_neighbors = 15
#clf = neighbors.KNeighborsClassifier(n_neighbors, weights=weights) #weights not defined
print clf
clf = clf.fit(train,target) 

print clf.coef_
print clf.intercept_

scores = cross_validation.cross_val_score(clf, train, target, cv=10)
print scores
print "Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() / 2)

raw_input();
savetxt('scores.csv', scores, delimiter=',', fmt='%f')
